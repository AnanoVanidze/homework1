package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        generateRandomNumberButton.setOnClickListener {
            val number: Int = randomNumber()
            d("Randomnumber", "This is random number ${number}")
            randomNumberTv.text = devidedFive(number)
        }
    }

    private fun randomNumber() = (-100..100).random()

    fun devidedFive(randomnumber: Int): String {
        if (randomnumber % 5 == 0 && randomnumber > 0) {
            return "yes"
        } else {
            return "no"
        }
    }


}